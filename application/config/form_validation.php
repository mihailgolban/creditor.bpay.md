<?php
/**
 * Created by PhpStorm.
 * User: mgolban
 * Date: 4/11/19
 * Time: 2:47 PM
 */

$config = array(
    'auth' => array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|required'
      ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required'
        )
    ),
);