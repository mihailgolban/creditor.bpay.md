<?php
/**
 * Created by PhpStorm.
 * User: mgolban
 * Date: 4/11/19
 * Time: 9:25 AM
 */

/**
 * Class Login
 * @property    CI_Form_validation form_validation
 * @property    CI_Session session
 * @property    CI_Lang lang
 * @property    CI_Input input
 * @property    User_model user_model
 */

class Auth extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'security', 'language'));
        $this->load->library('form_validation');
        $this->lang->load('auth');
    }

    public function index() {
        return $this->login();
    }

    public function login() {
        if ($this->session->userdata('logged_in') == true) {
            $this->redirectToHome();
        } else if ($this->form_validation->run('auth') == false) {
            $page_data['error_message'] = validation_errors();
            $this->load->view('auth/login', $page_data);
        } else {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $this->load->model('user_model');
            $query = $this->user_model->does_user_exist($username);
            if ($query->num_rows() == 1) {
                foreach ($query->result() as $row) {
                    if ($row->is_disabled == 0) {
                        if (password_verify($password, $row->password) == true) {
                            $this->setUserData($row);
                            $this->redirectToHome();
                        } else {
                            $page_data['error_message'] = 'Incorrect username or password';
                            $this->load->view('auth/login', $page_data);
                        }
                    } else {
                        $page_data['error_message'] = lang('login_unsuccessful_not_active');
                        $this->load->view('auth/login', $page_data);
                    }
                }
            } else {
                $page_data['error_message'] = 'Incorrect username or password';
                $this->load->view('auth/login', $page_data);
            }
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        $page_data['success_message'] = lang('logout_successful');
        $this->load->view('auth/login', $page_data);
    }

    private function setUserData($user) {
        $session_data = array(
            'user_id' => $user->user_id,
            'creditor_id' => $user->creditor_id,
            'creditor_name' => $this->user_model->getUserCreditor($user->user_id),
            'role_id' => $user->role_id,
            'role_name' => $this->user_model->getUserRole($user->user_id),
            'display_name' => $user->first_name . ' ' . $user->last_name,
            'email' => $user->email,
            'logged_in' => true
        );
        $this->session->set_userdata($session_data);
    }

    private function redirectToHome() {
        $page_data['success_message'] = "hz";
        if(!isset($_SESSION['role_id']))
            $this->load->view('auth/login', $page_data);
        switch ($_SESSION['role_id']) {
            case 0: //Operator
                redirect('loanApplications');
                break;
            case 1: //Manager
                redirect('loanApplications');
                break;
            case 2: //Administrator
                redirect('loanApplications');
        }
    }

    public function password_recovery() {
        $this->load->view('auth/password_recovery');

//        TODO send email
    }

}