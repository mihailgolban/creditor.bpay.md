<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: mgolban
 * Date: 4/8/19
 * Time: 4:21 PM
 */


class LoanApplications extends MY_Controller
{
    public function index() {
        $this->page_data['page_title'] = 'Loan Applications';
        $this->load->view('loanApplications', $this->page_data);
    }
}