<?php
/**
 * Created by PhpStorm.
 * User: mgolban
 * Date: 4/15/19
 * Time: 10:27 AM
 */

/**
 * Class UserManagement
 * @property    User_model user_model
 * @property    Role_model role_model
 * @property    Creditor_model creditor_model
 * @property    CI_Input   input
 * @property    CI_Output  output
 */
class UserManagement extends MY_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('role_model');
        $this->load->model('creditor_model');
    }

    function index() {
        $this->page(0);
    }

    function page($offset = 0) {
        $this->page_data['page_title'] = 'User management';
        $this->page_data['columns'] = array('First name', 'Last name', 'Username', 'Email', 'Role', 'Creditor', 'Disabled', 'Created at', 'Action');

//        Set filters
        $search = array('1' => '1');       // if search input is empty
        $selectors = array('1' => '1');    // if selector role and creditor input are empty
        if (isset($_GET) && !empty($_GET)) {
            if (isset($_GET['filterSearch']) && !empty($_GET['filterSearch'])) {
                $search = array(
                    'first_name' => $_GET['filterSearch'],
                    'last_name' => $_GET['filterSearch'],
                    'username' => $_GET['filterSearch'],
                    'email' => $_GET['filterSearch'],
                    );
            }
            if (isset($_GET['filterRole']) && !empty($_GET['filterRole']))
                $selectors['users.role_id'] = $_GET['filterRole'];
            if (isset($_GET['filterCreditor']) && !empty($_GET['filterCreditor']))
                $selectors['users.creditor_id'] = $_GET['filterCreditor'];
        }

        $this->page_data['offset'] = $offset;
        $this->page_data['limit'] = 3; // item per page
        $this->page_data['users'] = $this->user_model->all($search, $selectors, $this->page_data['limit'], $offset);
        $this->page_data['roles'] = $this->role_model->all();
        $this->page_data['creditors'] = $this->creditor_model->all();
        $this->page_data['total_rows'] = $this->user_model->getCountAll($search, $selectors);
        $this->page_data['pagination'] = $this->createPagination($this->page_data['limit']);
        $this->load->view('userManagement', $this->page_data);
    }

    function deleteUser() {
        if ($this->input->is_ajax_request()) {
            $success = false;
            if (isset($_POST['delete_username']) && $this->user_model->delete($_POST['delete_username']) != false)
                $success = true;
            return $this->output->set_content_type('application/json')
                                ->set_output(json_encode(array('success' => $success)));
        }
    }

    function editUser() {
        if ($this->input->is_ajax_request()) {
            $success = false;
            if (isset($_POST) && !empty($_POST)) {
                $data['user_id'] = $_POST['userID'];
                $data['first_name'] = $_POST['firstName'];
                $data['last_name'] = $_POST['lastName'];
                $data['username'] = $_POST['username'];
                $data['email'] = $_POST['email'];
                $data['role_id'] = $_POST['role'];
                $data['creditor_id'] = $_POST['creditor'];
                $data['is_disabled'] = $_POST['disabled'];
                if ($this->user_model->edit($data) == true) {
                    $success= true;
                }
            }
            return $this->output->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $success)));
        }
    }

    function addUser() {
        if ($this->input->is_ajax_request()) {
            $success = false;
            if (isset($_POST) && !empty($_POST)) {
                $data['user_id'] = $_POST['userID'];
                $data['first_name'] = $_POST['firstName'];
                $data['last_name'] = $_POST['lastName'];
                $data['username'] = $_POST['username'];
                $data['email'] = $_POST['email'];
                $data['role_id'] = $_POST['role'];
                $data['creditor_id'] = $_POST['creditor'];
                $data['is_disabled'] = $_POST['disabled'];
                if ($this->user_model->add($data) == true) {
                    $success= true;
                }
            }
            return $this->output->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $success)));
        }
    }
}