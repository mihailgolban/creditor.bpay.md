<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: mgolban
 * Date: 4/8/19
 * Time: 4:41 PM
 */

/**
 * Class MY_Controller
 * @property CI_URI uri
 * @property MY_Pagination pagination
 */

class MY_Controller extends CI_Controller
{
    protected $page_data;
    protected $has_permission = true;
    private $acl = array(
//        1 => Operator
//        2 => Manager
//        3 => Administrator
        'loanApplications/index' => array(3)
    );

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('pagination');
        if(!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] != true)
            redirect('auth/login');
        $this->has_permission = $this->hasPermission();
    }

    function hasPermission() {
        $class = $this->uri->segment(1) ? $this->uri->segment(1) : '';
        $method = $this->uri->segment(2) ? $this->uri->segment(2) : 'index';
        if (isset($this->acl[$class .'/'. $method])) {
            if (isset($_SESSION['role_id'])) {
                return in_array($_SESSION['role_id'], $this->acl[$class .'/'. $method]);
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function createPagination($per_page = false) {
        $config['base_url'] = base_url() . '/' . $this->uri->segment(1) . '/page/';
        $config['total_rows'] = $this->page_data['total_rows'] ?? 0;
        $config['per_page'] = empty($per_page) ? 20 : (int)$per_page;
        $config['reuse_query_string'] = true;
        $config['prev_link'] = 'Previous';
        $config['next_link'] = 'Next';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['prev_tag_open'] = '<li class="paginate_button previous">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="paginate_button next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="paginate_button active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="paginate_button ">';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        return $this->pagination->my_create_links();
    }
}