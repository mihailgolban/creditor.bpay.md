<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 2019-04-16
 * Time: 22:14
 */

class MY_Pagination extends CI_Pagination
{
    function __construct(array $params = array()) {
        parent::__construct($params);
    }

    function my_create_links() {
        $output = $this->create_links();

        // Calculate the total number of pages
        $num_pages = (int) ceil($this->total_rows / $this->per_page);

        // Render the "Previous" link.
        if ($this->prev_link !== FALSE && $this->cur_page == 1) {
            $output = '<li class="paginate_button previous disabled"><a href="#">' . $this->prev_link . '</a></li>' . $output;
        }
        // Render the "next" link
        if ($this->next_link !== FALSE && $this->cur_page == $num_pages) {
            $output .= '<li class="paginate_button next disabled"><a href="#">' . $this->next_link . '</a></li>';
        }

        return $output;
    }
}