<?php
/**
 * Created by PhpStorm.
 * User: mgolban
 * Date: 4/16/19
 * Time: 12:19 PM
 */

/**
 * Class Creditor_model
 * @property CI_DB_query_builder db
 */
class Creditor_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function find($id) {
        return $this->db->get_where('creditors', array('creditor_id' => $id))->row();
    }

    public function all() {
        return $this->db->get('creditors')->result();
    }

    public function add($data) {
        return $this->db->insert('creditors', $data);
    }

    public function edit($data) {
        return $this->db->update('creditors', $data, array('creditor_id' => $data['creditor_id']));
    }

    public function delete($id) {
        return $this->db->delete('creditors', array('creditor_id' => $id));
    }
}