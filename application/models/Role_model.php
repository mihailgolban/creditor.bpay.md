<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: mgolban
 * Date: 4/12/19
 * Time: 1:15 PM
 */

/**
 * Class Role
 * @property CI_DB_query_builder db
 */
class Role_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function find($id) {
        return $this->db->get_where('roles', array('role_id' => $id))->row();
    }

    public function all() {
        return $this->db->get('roles')->result();
    }

    public function add($data) {
        return $this->db->insert('roles', $data);
    }

    public function edit($data) {
        return $this->db->update('roles', $data, array('role_id' => $data['role_id']));
    }

    public function delete($id) {
        return $this->db->delete('roles', array('role_id' => $id));
    }
}