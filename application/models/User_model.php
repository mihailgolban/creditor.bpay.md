<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: mgolban
 * Date: 4/11/19
 * Time: 9:53 AM
 */

/**
 * Class Users_model
 * @property CI_DB_query_builder db
 */
class User_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function find($user_id) {
        return $this->db->get_where('users', array('user_id' => $user_id))->row();
    }

    public function all($search, $selectors, $limit, $offset) {
        return $this->db->join('roles', 'users.role_id=roles.role_id')
                        ->join('creditors', 'users.creditor_id=creditors.creditor_id')
                        ->like($selectors,'','none')
                        ->group_start()
                        ->or_like($search)
                        ->group_end()
                        ->get('users', $limit, $offset)->result();
    }

    public function getCountAll($search, $selectors) {
        return $this->db->like($selectors,'','none')
                        ->group_start()
                        ->or_like($search)
                        ->group_end()
                        ->count_all_results('users');
    }

    public function add($data) {
        return $this->db->insert('users', $data);
    }

    public function edit($data) {
        return $this->db->update('users', $data, array('user_id' => $data['user_id']));
    }

    public function delete($username) {
        return $this->db->delete('users', array('username' => $username));
    }

    public function getUserRole($user_id) {
        return $this->db->select('role_name')
                        ->join('roles', 'users.role_id = roles.role_id')
                        ->get_where('users', array('user_id' => $user_id))
                        ->row()->role_name;
    }

    public function getUserCreditor($user_id) {
        return $this->db->select('creditor_name')
                        ->join('creditors', 'users.creditor_id = creditors.creditor_id')
                        ->get_where('users', array('user_id' => $user_id))
                        ->row()->creditor_name;
    }



    function generate_forgotten_password_code() {
        do {
            $url_code = random_string('alnum', 40);
            $num = $this->db->where('forgotten_password_code', $url_code)
                            ->from('users')
                            ->count_all_results();
        } while ($num >= 1);
        return $url_code;
    }

    function update_user_code($data) {
        return $this->db->where('email', $data['email'])
            ->update('users', $data);
    }

    function update_user_password($data) {
        return $this->db->where('user_id', $data['user_id'])
                        ->update('users', $data);
    }

    function does_code_match($code, $email) {
        $this->db->where(array('forgot_password_code' => $code, 'email' => $email))->from('users');
        $res = $this->db->count_all_results();
        return ($res == 1) ? true : false;
    }

    function does_user_exist($username) {
        return $this->db->get_where('users',array('username' => $username));
    }
}