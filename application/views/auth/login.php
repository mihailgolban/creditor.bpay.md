<?php $this->load->view('common/header'); ?>
<div id="single-wrapper">
    <?php echo form_open("/auth/login", array('class' => 'frm-single'));?>
        <div class="inside">
            <div class="title"><strong>Credit</strong>Admin</div>
            <!-- /.title -->
            <div class="frm-title"><?php echo lang('login_heading');?></div>
            <!-- /.frm-title -->
            <?php if (isset($error_message) && !empty($error_message)):?>
                <div class="alert alert-danger" role="alert"> <?php echo $error_message ?> </div>
            <?php endif;?>
            <?php if (isset($success_message) && !empty($success_message)): ?>
                <div class="alert alert-success" role="alert"> <?php echo $success_message ?> </div>
            <?php endif; ?>
            <div class="frm-input">
                <?php echo form_input('username', set_value('username'), array('placeholder' => 'Username', 'class' => 'frm-inp'));?>
                <i class="fa fa-user frm-ico"></i>
            </div>
            <!-- /.frm-input -->
            <div class="frm-input">
                <?php echo form_password('password', '', array('placeholder' => 'Password', 'class' => 'frm-inp'));?>
<!--                <input type="password" placeholder="Password" class="frm-inp">-->
                <i class="fa fa-lock frm-ico"></i>
            </div>
            <!-- /.frm-input -->
            <div class="clearfix margin-bottom-20">
                <div class="pull-left">
                    <div class="checkbox primary">
                        <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
                        <?php echo form_label('Remember me', 'remember')?>
                    </div>
                    <!-- /.checkbox -->
                </div>
                <!-- /.pull-left -->
                <div class="pull-right"><a href="/auth/password_recovery" class="a-link"><i class="fa fa-unlock-alt"></i><?php echo lang('login_forgot_password');?></a></div>
                <!-- /.pull-right -->
            </div>
            <!-- /.clearfix -->
            <button type="submit" class="frm-submit"><?php echo lang('login_submit_btn') ?><i class="fa fa-arrow-circle-right"></i></button>
            <!-- /.row -->
            <div class="frm-footer">CreditAdmin © 2019.</div>
            <!-- /.footer -->
        </div>
        <!-- .inside -->
    <?php echo form_close(); ?>
    <!-- /.frm-single -->
</div><!--/#single-wrapper -->
<?php $this->load->view('common/footer'); ?>