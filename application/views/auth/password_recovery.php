<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('common/header'); ?>

<div id="single-wrapper">
	<form action="#" class="frm-single">
		<div class="inside">
			<div class="title"><strong>Credit</strong>Admin</div>
			<!-- /.title -->
			<div class="frm-title">Reset Password</div>
			<!-- /.frm-title -->
			<p class="text-center">Enter your email address and we'll send you an email with instructions to reset your password.</p>
			<div class="frm-input"><input type="email" placeholder="Enter Email" class="frm-inp"><i class="fa fa-envelope frm-ico"></i></div>
			<!-- /.frm-input -->
			<button type="submit" class="frm-submit">Send Email<i class="fa fa-arrow-circle-right"></i></button>
			<a href="/auth/login" class="a-link"><i class="fa fa-sign-in"></i>Already have account? Login.</a>
			<div class="frm-footer">CreditAdmin © 2019.</div>
			<!-- /.footer -->
		</div>
		<!-- .inside -->
	</form>
	<!-- /.frm-single -->
</div><!--/#single-wrapper -->

<?php $this->load->view('common/footer'); ?>