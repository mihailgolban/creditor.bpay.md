<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="<?php echo base_url('script/html5shiv.min.js'); ?>"></script>
<script src="<?php echo base_url('script/respond.min.js'); ?>"></script>
<![endif]-->
<!--
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url('scripts/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('scripts/modernizr.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/nprogress/nprogress.js'); ?>"></script>
<script src="<?php echo base_url('plugin/sweetalert2/dist/sweetalert2.all.min.js'); ?>"></script>

<script src="<?php echo base_url('plugin/waves/waves.min.js'); ?>"></script>

<!-- Data Tables -->
<script src="<?php echo base_url('plugin/datatables/media/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/datatables/media/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/datatables/extensions/Responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/datatables/extensions/Responsive/js/responsive.bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('scripts/datatables.min.js'); ?>"></script>

<!-- Morris Chart -->
<script src="<?php echo base_url('plugin/chart/morris/morris.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/chart/morris/raphael-min.js'); ?>"></script>
<script src="<?php echo base_url('scripts/chart.morris.init.min.js'); ?>"></script>

<!-- Flot Chart -->
<script src="<?php echo base_url('plugin/chart/plot/jquery.flot.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/chart/plot/jquery.flot.tooltip.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/chart/plot/jquery.flot.categories.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/chart/plot/jquery.flot.pie.min.js'); ?>"></script>
<script src="<?php echo base_url('plugin/chart/plot/jquery.flot.stack.min.js'); ?>"></script>
<script src="<?php echo base_url('scripts/chart.flot.init.min.js'); ?>"></script>

<!-- Sparkline Chart -->
<script src="<?php echo base_url('plugin/chart/sparkline/jquery.sparkline.min.js'); ?>"></script>
<script src="<?php echo base_url('scripts/chart.sparkline.init.min.js'); ?>"></script>

<!-- FullCalendar -->
<script src="<?php echo base_url('plugin/moment/moment.js'); ?>"></script>
<script src="<?php echo base_url('plugin/fullcalendar/fullcalendar.min.js'); ?>"></script>
<script src="<?php echo base_url('scripts/fullcalendar.init.js'); ?>"></script>

<!-- Remodal -->
<script src="<?php echo base_url('plugin/modal/remodal/remodal.min.js')?>"></script>

<!-- X-Editable -->
<script src="<?php echo base_url('plugin/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js')?>"></script>

<!-- Validator -->
<script src="<?php echo base_url('plugin/validator/validator.min.js')?>"></script>

<script src="<?php echo base_url('scripts/main.js'); ?>"></script>

<!--<script src="--><?php //echo base_url('scripts/react/dist/bundle.js'); ?><!--"></script>-->

</body>
</html>