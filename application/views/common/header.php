<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $page_title; ?></title>

    <!-- Main Styles -->
    <link rel="stylesheet" href="<?php echo base_url('styles/style.min.css');?>">

    <!-- Material Design Icon -->
    <link rel="stylesheet" href="<?php echo base_url('fonts/material-design/css/materialdesignicons.css');?>">

    <!-- mCustomScrollbar -->
    <link rel="stylesheet" href="<?php echo base_url('plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css');?>">

    <!-- Waves Effect -->
    <link rel="stylesheet" href="<?php echo base_url('plugin/waves/waves.min.css');?>">

    <!-- Sweet Alert -->
    <link rel="stylesheet" href="<?php echo base_url('plugin/sweetalert2/sweetalert2-custom.min.css')?>">

    <!-- Data Tables -->
    <link rel="stylesheet" href="<?php echo base_url('plugin/datatables/media/css/dataTables.bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css');?>">

    <!-- Morris Chart -->
    <link rel="stylesheet" href="<?php echo base_url('plugin/chart/morris/morris.css');?>">

    <!-- FullCalendar -->
    <link rel="stylesheet" href="<?php echo base_url('plugin/fullcalendar/fullcalendar.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('plugin/fullcalendar/fullcalendar.print.css');?>" media='print'>

    <!-- Remodal -->
    <link rel="stylesheet" href="<?php echo base_url('plugin/modal/remodal/remodal.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('plugin/modal/remodal/remodal-default-theme.css'); ?>">

    <!-- X-Editable -->
    <link rel="stylesheet" href="<?php echo base_url('plugin/x-editable/bootstrap3-editable/css/bootstrap-editable.css'); ?>">

</head>

<body>