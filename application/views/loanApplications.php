<?php $this->load->view('common/header'); ?>
<?php $this->load->view('nav/main_menu'); ?>
<?php $this->load->view('nav/navbar'); ?>
<div id="wrapper">
    <div class="main-content">
        <div class="row small-spacing">
            <div class="col-xs-12">
                <div class="box-content">
                    <h4 class="box-title">Applicants</h4>
                    <div id="react">a</div>
                    <!-- /.box-title -->

                    <table id="loanApplications" class="table table-striped table-bordered display" style="width:100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>IDNP</th>
                            <th>Birthday</th>
                            <th>Country</th>
                            <th>Phone number</th>
                            <th>Decision</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>IDNP</th>
                            <th>Birthday</th>
                            <th>Country</th>
                            <th>Phone number</th>
                            <th>Decision</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <tr>
                            <td>Jack Charles</td>
                            <td>1624020119899</td>
                            <td>02.12.20</td>
                            <td>Congo, the Democratic Republic of the</td>
                            <td>1-488-665-7843</td>
                            <td style="color: red;">Review1<a href="#">

                                    <i class="fa fa-pencil"></i></a></td>
                        </tr>
                        <tr>
                            <td>Tarik Cannon</td>
                            <td>1650071594199</td>
                            <td>06.05.19</td>
                            <td>Holy See (Vatican City State)</td>
                            <td>308-3447</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Zachary Copeland</td>
                            <td>1602112893099</td>
                            <td>01.06.19</td>
                            <td>Czech Republic</td>
                            <td>726-6370</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Ezra Mosley</td>
                            <td>1608081972099</td>
                            <td>03.08.20</td>
                            <td>Hungary</td>
                            <td>1-904-752-1294</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Mannix Aguirre</td>
                            <td>1602102866199</td>
                            <td>09.30.19</td>
                            <td>Azerbaijan</td>
                            <td>1-115-173-3533</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Guy Hudson</td>
                            <td>1647071606999</td>
                            <td>08.11.19</td>
                            <td>Portugal</td>
                            <td>1-149-328-1302</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Nissim Gonzalez</td>
                            <td>1634122100999</td>
                            <td>12.29.19</td>
                            <td>Bermuda</td>
                            <td>1-230-568-8395</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Alec Whitfield</td>
                            <td>1625052148299</td>
                            <td>12.15.19</td>
                            <td>Anguilla</td>
                            <td>1-370-164-6374</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Guy Torres</td>
                            <td>1633081648599</td>
                            <td>10.12.19</td>
                            <td>United Kingdom (Great Britain)</td>
                            <td>810-5220</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Blake Hays</td>
                            <td>1633011191199</td>
                            <td>11.30.19</td>
                            <td>Saint Kitts and Nevis</td>
                            <td>1-507-658-2088</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Caleb Leach</td>
                            <td>1691073020899</td>
                            <td>11.17.19</td>
                            <td>Liechtenstein</td>
                            <td>1-249-817-6932</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Barry Bennett</td>
                            <td>1631102837599</td>
                            <td>07.08.19</td>
                            <td>Belize</td>
                            <td>647-9331</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Hilel Workman</td>
                            <td>1689011892899</td>
                            <td>03.09.20</td>
                            <td>Panama</td>
                            <td>1-403-385-6911</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Bradley Shepard</td>
                            <td>1674042161199</td>
                            <td>03.01.20</td>
                            <td>Christmas Island</td>
                            <td>1-569-696-5042</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Cody Neal</td>
                            <td>1616012090299</td>
                            <td>10.10.19</td>
                            <td>Saint Kitts and Nevis</td>
                            <td>1-950-442-6006</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Melvin Duffy</td>
                            <td>1694090114099</td>
                            <td>02.08.19</td>
                            <td>Tonga</td>
                            <td>535-9040</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Dennis Stuart</td>
                            <td>1631011226299</td>
                            <td>07.12.18</td>
                            <td>Isle of Man</td>
                            <td>907-9167</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Deacon Mccoy</td>
                            <td>1692010698799</td>
                            <td>02.25.20</td>
                            <td>Uruguay</td>
                            <td>1-242-239-2581</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Colton Kinney</td>
                            <td>1612010422099</td>
                            <td>07.21.18</td>
                            <td>Mali</td>
                            <td>1-941-612-8055</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Jerome Spence</td>
                            <td>1673020197599</td>
                            <td>06.13.19</td>
                            <td>Maldives</td>
                            <td>360-0419</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Perry Hendrix</td>
                            <td>1673112522899</td>
                            <td>12.19.19</td>
                            <td>France</td>
                            <td>907-3042</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Boris Wilson</td>
                            <td>1683110322199</td>
                            <td>05.09.19</td>
                            <td>Equatorial Guinea</td>
                            <td>533-6102</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Prescott Anthony</td>
                            <td>1696102108699</td>
                            <td>08.26.18</td>
                            <td>Trinidad and Tobago</td>
                            <td>1-450-944-5370</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Abbot Kline</td>
                            <td>1628021636699</td>
                            <td>06.06.18</td>
                            <td>Oman</td>
                            <td>805-8290</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Reece Howard</td>
                            <td>1678092803799</td>
                            <td>02.20.20</td>
                            <td>Botswana</td>
                            <td>157-4235</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Yardley Griffith</td>
                            <td>1623031942299</td>
                            <td>09.16.18</td>
                            <td>Ethiopia</td>
                            <td>1-982-178-4107</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Abel Dean</td>
                            <td>1601050201699</td>
                            <td>06.01.18</td>
                            <td>Israel</td>
                            <td>1-897-958-3425</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Dean Landry</td>
                            <td>1635082193199</td>
                            <td>05.27.18</td>
                            <td>Indonesia</td>
                            <td>483-7149</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Dennis Schneider</td>
                            <td>1640071216799</td>
                            <td>04.01.19</td>
                            <td>Belize</td>
                            <td>1-861-299-8040</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Joseph Stafford</td>
                            <td>1657070925899</td>
                            <td>07.30.19</td>
                            <td>Saint Pierre and Miquelon</td>
                            <td>494-7911</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Rafael Gutierrez</td>
                            <td>1663071714299</td>
                            <td>09.11.18</td>
                            <td>Bosnia and Herzegovina</td>
                            <td>1-942-537-0903</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Talon Bolton</td>
                            <td>1637101407199</td>
                            <td>11.04.18</td>
                            <td>Malaysia</td>
                            <td>1-627-462-5238</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Sawyer Gentry</td>
                            <td>1604090367999</td>
                            <td>05.08.18</td>
                            <td>Chad</td>
                            <td>1-648-542-8773</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Clarke Gray</td>
                            <td>1632022507599</td>
                            <td>03.05.20</td>
                            <td>Mayotte</td>
                            <td>102-4225</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Amal Noel</td>
                            <td>1629092170999</td>
                            <td>03.11.20</td>
                            <td>Trinidad and Tobago</td>
                            <td>1-716-388-8234</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Beau Stanton</td>
                            <td>1675061797599</td>
                            <td>05.22.18</td>
                            <td>Reunion</td>
                            <td>456-3011</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Nicholas Church</td>
                            <td>1684121226899</td>
                            <td>12.23.18</td>
                            <td>South Georgia and The South Sandwich Islands</td>
                            <td>599-8372</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Finn Osborn</td>
                            <td>1675050275099</td>
                            <td>10.22.18</td>
                            <td>Taiwan</td>
                            <td>546-1983</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Kibo Monroe</td>
                            <td>1671020481299</td>
                            <td>06.23.19</td>
                            <td>Maldives</td>
                            <td>392-6408</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Rudyard Harrell</td>
                            <td>1691030294499</td>
                            <td>09.08.19</td>
                            <td>Bhutan</td>
                            <td>1-283-956-2909</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Burton Greer</td>
                            <td>1680102234899</td>
                            <td>09.27.19</td>
                            <td>India</td>
                            <td>368-6863</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Chester Riddle</td>
                            <td>1652042923699</td>
                            <td>02.04.19</td>
                            <td>Bangladesh</td>
                            <td>1-866-375-9549</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Ryder Cline</td>
                            <td>1667040804199</td>
                            <td>02.01.20</td>
                            <td>Greece</td>
                            <td>394-5811</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Harrison Snyder</td>
                            <td>1674100475499</td>
                            <td>10.29.19</td>
                            <td>Comoros</td>
                            <td>1-749-791-0915</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Elliott Francis</td>
                            <td>1622071553699</td>
                            <td>08.23.18</td>
                            <td>Luxembourg</td>
                            <td>212-9745</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Nehru Cooper</td>
                            <td>1609011854599</td>
                            <td>05.02.19</td>
                            <td>Uruguay</td>
                            <td>1-595-549-1043</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Daquan Osborn</td>
                            <td>1628013051699</td>
                            <td>05.01.18</td>
                            <td>Tanzania</td>
                            <td>363-1730</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Baxter Sargent</td>
                            <td>1651031711699</td>
                            <td>11.20.19</td>
                            <td>Pakistan</td>
                            <td>618-7609</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Luke Greer</td>
                            <td>1633071544299</td>
                            <td>04.09.19</td>
                            <td>France</td>
                            <td>488-2049</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Leonard Wise</td>
                            <td>1656051796599</td>
                            <td>04.07.20</td>
                            <td>Panama</td>
                            <td>1-442-373-6462</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Drew Woodard</td>
                            <td>1654060998699</td>
                            <td>06.01.19</td>
                            <td>Turkey</td>
                            <td>987-5172</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Quinlan Mendoza</td>
                            <td>1631031321899</td>
                            <td>11.07.18</td>
                            <td>United States Minor Outlying Islands</td>
                            <td>1-430-488-0495</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Graham Frazier</td>
                            <td>1639082938799</td>
                            <td>04.29.19</td>
                            <td>Saudi Arabia</td>
                            <td>520-5596</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Gareth Dixon</td>
                            <td>1659030799099</td>
                            <td>08.11.18</td>
                            <td>Cuba</td>
                            <td>737-6873</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Levi Anderson</td>
                            <td>1664032410899</td>
                            <td>02.20.20</td>
                            <td>Algeria</td>
                            <td>1-997-480-6759</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Kenyon Horton</td>
                            <td>1652051181099</td>
                            <td>05.28.18</td>
                            <td>Côte D'Ivoire (Ivory Coast)</td>
                            <td>685-0796</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Sawyer Prince</td>
                            <td>1660042100699</td>
                            <td>12.06.18</td>
                            <td>Saint Kitts and Nevis</td>
                            <td>419-5980</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Malachi Hays</td>
                            <td>1667101614599</td>
                            <td>08.05.19</td>
                            <td>Palestine, State of</td>
                            <td>1-617-442-2965</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Amos Nielsen</td>
                            <td>1659102387099</td>
                            <td>12.19.18</td>
                            <td>Libya</td>
                            <td>956-6137</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Baker Acosta</td>
                            <td>1624012573199</td>
                            <td>06.01.19</td>
                            <td>Cameroon</td>
                            <td>306-0204</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Vernon Hunter</td>
                            <td>1610090880199</td>
                            <td>02.06.19</td>
                            <td>Saint Vincent and The Grenadines</td>
                            <td>1-213-941-0027</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Vladimir Shaffer</td>
                            <td>1631012872699</td>
                            <td>11.22.18</td>
                            <td>Switzerland</td>
                            <td>1-482-400-6528</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Benjamin Hooper</td>
                            <td>1619031985499</td>
                            <td>03.24.19</td>
                            <td>Chile</td>
                            <td>956-5327</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Dale Wallace</td>
                            <td>1690061102099</td>
                            <td>10.28.19</td>
                            <td>Tokelau</td>
                            <td>1-208-183-1593</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Dale Sparks</td>
                            <td>1603063035699</td>
                            <td>08.16.19</td>
                            <td>Angola</td>
                            <td>505-4107</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Ethan Sykes</td>
                            <td>1674041342199</td>
                            <td>01.21.19</td>
                            <td>Poland</td>
                            <td>222-4128</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Mohammad Duncan</td>
                            <td>1641090436899</td>
                            <td>02.25.19</td>
                            <td>Canada</td>
                            <td>1-201-883-6272</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Lucian Joseph</td>
                            <td>1646101628699</td>
                            <td>10.16.18</td>
                            <td>Palau</td>
                            <td>839-4788</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Owen Bennett</td>
                            <td>1682090588599</td>
                            <td>07.30.18</td>
                            <td>Kiribati</td>
                            <td>1-642-793-6000</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Fulton Lang</td>
                            <td>1668092222699</td>
                            <td>07.19.19</td>
                            <td>Malaysia</td>
                            <td>1-303-383-1143</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Hamilton England</td>
                            <td>1698112989899</td>
                            <td>02.14.19</td>
                            <td>France</td>
                            <td>1-128-799-2358</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Oscar Bird</td>
                            <td>1639050888499</td>
                            <td>11.17.18</td>
                            <td>Belize</td>
                            <td>1-685-134-1081</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Hyatt Higgins</td>
                            <td>1622122074899</td>
                            <td>10.17.18</td>
                            <td>Italy</td>
                            <td>776-8974</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Elliott Jimenez</td>
                            <td>1637102995399</td>
                            <td>10.03.19</td>
                            <td>Holy See (Vatican City State)</td>
                            <td>731-5638</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Macaulay Harris</td>
                            <td>1678010203499</td>
                            <td>07.02.18</td>
                            <td>Anguilla</td>
                            <td>1-394-642-8818</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Philip Pickett</td>
                            <td>1621082015699</td>
                            <td>09.24.18</td>
                            <td>Tuvalu</td>
                            <td>485-6574</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Alexander Sargent</td>
                            <td>1680110580099</td>
                            <td>02.06.20</td>
                            <td>Ecuador</td>
                            <td>185-7451</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Giacomo Ewing</td>
                            <td>1636061968799</td>
                            <td>07.26.19</td>
                            <td>Belize</td>
                            <td>1-776-948-6904</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Christopher Maynard</td>
                            <td>1610011760399</td>
                            <td>06.10.19</td>
                            <td>Venezuela</td>
                            <td>1-302-774-7358</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Hayden Walter</td>
                            <td>1630040378699</td>
                            <td>10.19.19</td>
                            <td>Rwanda</td>
                            <td>967-5714</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Levi Potter</td>
                            <td>1633031022699</td>
                            <td>03.01.20</td>
                            <td>Nicaragua</td>
                            <td>525-6654</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Jamal Logan</td>
                            <td>1645043091299</td>
                            <td>04.05.20</td>
                            <td>Gibraltar</td>
                            <td>189-3281</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Colby Keller</td>
                            <td>1625052826799</td>
                            <td>01.23.19</td>
                            <td>Zimbabwe</td>
                            <td>1-932-254-1122</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Nero Russo</td>
                            <td>1643111019699</td>
                            <td>05.30.18</td>
                            <td>Anguilla</td>
                            <td>488-4865</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Preston Wooten</td>
                            <td>1606040948999</td>
                            <td>01.05.19</td>
                            <td>Venezuela</td>
                            <td>1-971-682-3844</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Kuame White</td>
                            <td>1688121050699</td>
                            <td>06.21.18</td>
                            <td>Mayotte</td>
                            <td>175-4189</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Jelani Price</td>
                            <td>1673122468899</td>
                            <td>03.23.19</td>
                            <td>Taiwan</td>
                            <td>1-388-874-2114</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Kyle Fowler</td>
                            <td>1697051373799</td>
                            <td>10.13.18</td>
                            <td>Saint Pierre and Miquelon</td>
                            <td>1-323-432-0223</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Acton Farrell</td>
                            <td>1630060692099</td>
                            <td>11.24.19</td>
                            <td>Moldova</td>
                            <td>1-405-296-9662</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Justin Yates</td>
                            <td>1642121116899</td>
                            <td>11.18.18</td>
                            <td>Czech Republic</td>
                            <td>1-397-833-2301</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Davis Emerson</td>
                            <td>1621050276799</td>
                            <td>11.16.19</td>
                            <td>Nepal</td>
                            <td>820-5574</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Mason Mercer</td>
                            <td>1629102543299</td>
                            <td>08.07.18</td>
                            <td>Azerbaijan</td>
                            <td>1-544-497-1741</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Lucius Jackson</td>
                            <td>1675032941399</td>
                            <td>07.03.19</td>
                            <td>Greece</td>
                            <td>1-674-981-6661</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Brendan Lott</td>
                            <td>1620122078699</td>
                            <td>08.13.18</td>
                            <td>Burkina Faso</td>
                            <td>1-517-459-0524</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Kadeem Luna</td>
                            <td>1695062821699</td>
                            <td>09.16.18</td>
                            <td>Malaysia</td>
                            <td>1-634-635-1605</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Acton Espinoza</td>
                            <td>1641020620099</td>
                            <td>08.02.18</td>
                            <td>Fiji</td>
                            <td>1-741-438-1273</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Brock Trujillo</td>
                            <td>1688021963399</td>
                            <td>01.09.19</td>
                            <td>South Sudan</td>
                            <td>424-6457</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>Avram Washington</td>
                            <td>1690010461899</td>
                            <td>05.05.18</td>
                            <td>Portugal</td>
                            <td>1-899-787-8351</td>
                            <td>Review</td>
                        </tr>
                        <tr>
                            <td>Kennedy Gay</td>
                            <td>1603102101599</td>
                            <td>05.17.19</td>
                            <td>Canada</td>
                            <td>809-8059</td>
                            <td>Rejected</td>
                        </tr>
                        <tr>
                            <td>Zephania Garcia</td>
                            <td>1621050316299</td>
                            <td>12.19.19</td>
                            <td>Montenegro</td>
                            <td>1-650-421-8183</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-content -->
            </div>
        </div>
    </div>
    <!-- /.main-content -->
</div><!--/#wrapper -->
<?php $this->load->view('common/footer'); ?>