<div class="main-menu">
    <header class="header">
        <a href="<?php echo base_url();?>" class="logo"><i class="ico mdi mdi-cube-outline"></i>CreditAdmin</a>
        <button type="button" class="button-close fa fa-times js__menu_close"></button>
        <div class="user">
            <a href="#" class="avatar"><img src="<?php echo base_url('images/avatar-sm-5.jpg')?>" alt=""><span class="status online"></span></a>
            <h5 class="name"><a href="profile.html"><?php echo isset($_SESSION['display_name']) ? $_SESSION['display_name'] : 'Credit Admin' ?></a></h5>
            <h5 class="position"><?php echo isset($_SESSION['role_name']) ? $_SESSION['role_name'] : 'User' ; ?></h5>
            <!-- /.name -->
            <div class="control-wrap js__drop_down">
                <i class="fa fa-caret-down js__drop_down_button"></i>
                <div class="control-list">
                    <div class="control-item"><a href="profile.html"><i class="fa fa-user"></i> Profile</a></div>
                    <div class="control-item"><a href="#"><i class="fa fa-gear"></i> Settings</a></div>
                    <div class="control-item"><a href="#"><i class="fa fa-sign-out"></i> Log out</a></div>
                </div>
                <!-- /.control-list -->
            </div>
            <!-- /.control-wrap -->
        </div>
        <!-- /.user -->
    </header>
    <!-- /.header -->
    <div class="content">

        <div class="navigation">
            <h5 class="title">Navigation</h5>
            <!-- /.title -->
            <ul class="menu js__accordion">
                <li>
                    <a class="waves-effect" href="<?php echo site_url();?>"><i class="menu-icon mdi mdi-view-dashboard"></i><span>Dashboard</span></a>
                    <a class="waves-effect" href="<?php echo site_url();?>"><i class="menu-icon mdi mdi-view-dashboard"></i><span>Credit Organizations</span></a>
                </li>
            </ul>
            <!-- /.menu js__accordion -->
        </div>
        <!-- /.navigation -->
    </div>
    <!-- /.content -->
</div>
<!-- /.main-menu -->