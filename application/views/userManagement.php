<?php $this->load->view('common/header'); ?>
<?php $this->load->view('nav/main_menu'); ?>
<?php $this->load->view('nav/navbar'); ?>
<div id="wrapper">
    <div class="main-content">
        <div class="row small-spacing">
            <div class="col-xs-12">
                <div class="box-content">
                    <div class="box-title">
                        <div class="filters">
                            <form method="get" id="formFilters" class="row no-margin">
                                <ul class="list-inline margin-bottom-0">
                                    <li class="form-group col-xs-12 col-sm-6 col-lg-3">
                                        <input type="text" class="form-control" name="filterSearch" placeholder="Search..."
                                               value="<?php echo isset($_GET['filterSearch']) ? $_GET['filterSearch'] : ""; ?>">
                                    </li>
                                    <li class="form-group col-xs-12 col-sm-3 col-lg-2">
                                            <select class="form-control" name="filterRole">
                                                <option value="">Nothing selected</option>
                                                <?php foreach ($roles as $role): ?>
                                                    <option value="<?php echo $role->role_id; ?>"
                                                            <?php echo (isset($_GET['filterRole']) && $_GET['filterRole'] == strval($role->role_id)) ? 'selected' : ''; ?>>
                                                        <?php echo $role->role_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                    </li>
                                    <li class="form-group col-xs-12 col-sm-3 col-lg-2">
                                        <select class="form-control" name="filterCreditor">
                                            <option value="">Nothing selected</option>
                                            <?php foreach ($creditors as $creditor): ?>
                                                <option value="<?php echo $creditor->creditor_id; ?>"
                                                        <?php echo (isset($_GET['filterCreditor']) && $_GET['filterCreditor'] == strval($creditor->creditor_id)) ? 'selected' : ''?>>
                                                    <?php echo $creditor->creditor_name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </li>
                                    <li class="form-group col-xs-12 col-sm-12 col-lg-5">
                                        <button type="submit" class="btn btn-violet waves-effect waves-light"><i class="ico ico-left fa fa-search"></i>Search</button>
                                        <button type="button" class="btn btn-primary pull-right waves-effect waves-light" onclick="addUser()">
                                                <i class="ico ico-left fa fa-plus"></i>Add user</button>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <?php foreach ($columns as $column): ?>
                                <th><?php echo $column ?></th>
                                <?php endforeach; ?>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($users as $user):?>
                                <tr>
                                    <td style="display: none"><?php echo $user->user_id       ?></td>
                                    <td><?php echo $user->first_name    ?></td>
                                    <td><?php echo $user->last_name     ?></td>
                                    <td class="username"><?php echo $user->username      ?></td>
                                    <td><?php echo $user->email         ?></td>
                                    <td><?php echo $user->role_name     ?></td>
                                    <td><?php echo $user->creditor_name ?></td>
                                    <td><?php echo $user->is_disabled ? 'Yes' : 'No'  ?></td>
                                    <td><?php echo $user->created_at    ?></td>
                                    <td><button type="button" class="btn btn-primary btn-xs waves-effect waves-light" onclick="editUser(this)">
                                            <i class="ico ico-left fa fa-edit"></i>Edit</button>
                                        <button type="button" class="btn btn-danger btn-xs waves-effect waves-light js__deleteUser" onclick="deleteUser(this)">
                                            <i class="ico ico-left fa fa-trash"></i>Delete</button>
                                    </td>
                                <?php endforeach; ?>
                                </tr>
                            </tbody>
                        </table>
                        <!--Pagination-->
                        <div class="row no-margin">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="example_info" role="status" aria-live="polite">
                                    Total: <?php echo $total_rows; ?> entries</div>
                            </div>
                            <div class="col-sm-7 text-right">
                                <ul class="pagination no-margin">
                                    <?php echo $pagination ?>
                                </ul>
                            </div>
                        </div>
                        <!--/.Pagination-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add/Edit User Form-->
<div data-remodal-id="remodal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
    <div class="remodal-content">
        <h3 id="formTitle">Edit user</h3>
        <form id="editUserForm" class="form-horizontal" data-toggle="validator" novalidate="true">
            <input type="hidden" id="inputUserID" value="">
            <div class="form-group has-error has-danger">
                <label for="inputFirstName" class="col-sm-2 control-label">First name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputFirstName" value="First name" required>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="form-group has-error has-danger">
                <label for="inputLastName" class="col-sm-2 control-label">Last name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputLastName" value="Last name" required>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="form-group has-error has-danger">
                <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputUsername" value="Username" required>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail">
                </div>
            </div>
            <div class="form-group">
                <label for="selectRole" class="col-sm-2 control-label">Role</label>
                <div class="col-sm-10">
                    <select class="form-control" id="selectRole">
                        <?php foreach ($roles as $role): ?>
                            <option value="<?php echo $role->role_id; ?>"><?php echo $role->role_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="selectCreditor" class="col-sm-2 control-label">Creditor</label>
                <div class="col-sm-10">
                    <select class="form-control" id="selectCreditor">
                        <?php foreach ($creditors as $creditor): ?>
                            <option value="<?php echo $creditor->creditor_id; ?>"><?php echo $creditor->creditor_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10 text-left">
                    <div class="checkbox">
                        <input type="checkbox" id="chk-disabled"> <label for="chk-disabled">Account is disabled</label>
                    </div>
                </div>
            </div>
            <button id="formEditUserBtn" class="btn btn-info btn-sm waves-effect waves-light" onclick="saveChanges()">Save changes</button>
            <button id="formAddUserBtn" class="btn btn-info btn-sm waves-effect waves-light" onclick="add()">Add user</button>
        </form>
    </div>
</div>
<!-- /.Edit User Form-->
<?php $this->load->view('common/footer'); ?>

<script type="text/javascript">

    function ajaxRequest(params) {
        $.ajax({
            url: params.url,
            type: params.method,
            data: params.data,
            dataType: params.dataType,
            success: function(data) {
                if (data.success === true) {
                    Swal.fire({
                        title: params.alert.title,
                        html:  params.alert.body,
                        type:  params.alert.type,
                        onAfterClose: () => {
                            document.location.reload(true);
                        }
                    })
                } else {
                    Swal.fire(
                        'Oops...',
                        'Something went wrong!.',
                        'error'
                    )
                }
            },
            error: function (data) {
                Swal.fire(
                    'Oops...',
                    'Something went wrong!.',
                    'error'
                )
            }
        });
    }

    function returnFormElements() {
        return {
            userID: document.getElementById('inputUserID'),
            title: document.getElementById('formTitle'),
            firstName: document.getElementById('inputFirstName'),
            lastName: document.getElementById('inputLastName'),
            username: document.getElementById('inputUsername'),
            email: document.getElementById('inputEmail'),
            role: document.getElementById('selectRole'),
            creditor: document.getElementById('selectCreditor'),
            disabled: document.getElementById('chk-disabled'),
            addButton: document.getElementById('formAddUserBtn'),
            editButton: document.getElementById('formEditUserBtn'),
        }
    }
    // Delete User
    function deleteUser(deleteBtn) {
        let parent = $(deleteBtn).closest('tr');
        let username = parent.children('.username').text();

        Swal.fire({
            title: "Are you sure?",
            html: "Do you really want to delete user " + username.bold() + "? This process cannot be undone.",
            type: "warning",
            showCancelButton: true,
            focusCancel: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: "Delete",
            reverseButtons: true,
            showLoaderOnConfirm: true,
            preConfirm: () => {
                Swal.getCancelButton().style.display = 'none';
                return new Promise(function () {
                   ajaxDeleteUser(username)
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Deleted!',
                    'User ' + username.bold() + ' has been deleted.',
                    'success'
                )
            }
        })
    }
    function ajaxDeleteUser(username) {
        let params = {
            url: '/userManagement/deleteUser',
            method: 'POST',
            data: {delete_username: username},
            dataType: 'json',
            alert: {
                title: 'Deleted!',
                body: 'User ' + username.bold() + ' has been deleted.',
                type:  'success'
            }
        };
        ajaxRequest(params);
    }
    // /Delete User
    
    // Edit User
    let formUserModal = $('[data-remodal-id=remodal]').remodal();
    let row;
    function editUser(editBtn){
        row = $(editBtn).closest('tr');
        let children = row.children();
        let userInfo = {};
        userInfo.userID = children.get(0).textContent;
        userInfo.firstName = children.get(1).textContent;
        userInfo.lastName = children.get(2).textContent;
        userInfo.username = children.get(3).textContent;
        userInfo.email = children.get(4).textContent;
        userInfo.role = children.get(5).textContent;
        userInfo.creditor = children.get(6).textContent;
        userInfo.disabled =  (children.get(7).textContent === 'Yes') ? 1 : 0;
        let editForm = returnFormElements();
        editForm.title.textContent = "Edit user";
        editForm.userID.value = userInfo.userID;
        editForm.firstName.value = userInfo.firstName;
        editForm.lastName.value = userInfo.lastName;
        editForm.username.value = userInfo.username;
        editForm.email.value = userInfo.email;
        $('#selectRole > option').each(function () {
            if ($(this).text() === userInfo.role) {
                $(this).attr("selected", "selected");
            } else {
                $(this).removeAttr("selected");
            }
        });
        $('#selectCreditor > option').each(function () {
            if ($(this).text() === userInfo.creditor) {
                $(this).attr("selected", "selected");
            } else {
                $(this).removeAttr("selected");
            }

        });
        if (userInfo.disabled === 1) {
            $('#chk-disabled').attr('checked', 'checked');
        } else {
            $('#chk-disabled').removeAttr('checked');
        }
        editForm.addButton.style.display = "none";
        editForm.editButton.style.display = "inline-block";
        formUserModal.open();
    }
    
    function saveChanges() {
        ajaxEditUser();
        formUserModal.close();
    }

    function ajaxEditUser() {
        let postData = {};
        let editForm = returnFormElements();
        postData.userID = editForm.userID.value;
        postData.firstName = editForm.firstName.value;
        postData.lastName = editForm.lastName.value;
        postData.username = editForm.username.value;
        postData.email = editForm.email.value;
        postData.role = editForm.role.value;
        postData.creditor = editForm.creditor.value;
        postData.disabled = editForm.disabled.checked ? 1 : 0;
        let params = {
          url: '/userManagement/editUser',
          method: 'POST',
          data: postData,
          dataType: 'json',
          alert: {
              title: 'Edited!',
              body: 'User has been edited.',
              type: 'success'
          }
        };
        ajaxRequest(params);
    }
    // /Edit User
    
    // Add User
    function addUser() {
        let addForm = returnFormElements();
        addForm.title.textContent = "Add user";
        addForm.firstName.value = "";
        addForm.lastName.value = "";
        addForm.username.value = "";
        addForm.email.value = "";
        addForm.role.value = 1;
        addForm.creditor.value = 1;
        addForm.disabled.cheched = false;
        addForm.addButton.style.display = "inline-block";
        addForm.editButton.style.display = "none";
        formUserModal.open();
    }

    function add() {
        ajaxAddUser();
        formUserModal.close();
    }

    function ajaxAddUser() {
        let postData = {};
        let addForm = returnFormElements();
        postData.userID = addForm.userID.value;
        postData.firstName = addForm.firstName.value;
        postData.lastName = addForm.lastName.value;
        postData.username = addForm.username.value;
        postData.email = addForm.email.value;
        postData.role = addForm.role.value;
        postData.creditor = addForm.creditor.value;
        postData.disabled = addForm.disabled.checked ? 1 : 0;
        let params = {
            url: '/userManagement/addUser',
            method: 'POST',
            data: postData,
            dataType: 'json',
            alert: {
                title: 'Added!',
                body: 'User ' + postData.username.bold() + ' has been successfully added!',
                type: 'success'
            }
        };
        ajaxRequest(params);
    }
    // /Add User
</script>