(function($) {
    if ($('#loanApplications').length)
        $('#loanApplications').DataTable({
            "columnDefs": [
                {"width":"15%", "targets": [0,1,2,3,4]}
            ]
        });
})(jQuery);