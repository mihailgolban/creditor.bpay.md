

import React from 'react'
import Table from 'react-bootstrap/Table'
import ReactDOM from 'react-dom'
import Button from './components/Button'

// class App extends React.Component {
//
//
// }
//
// export default App

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }
    componentDidMount() {
        this.timerID = setInterval(
            ()=>this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
           date: new Date()
        });
    }

    render() {
        return (
            <div>
                <h1>Hello, world!

                <Button/>
                </h1>
                <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
            </div>
        );
    }
}

ReactDOM.render(
    <Clock/>,
    document.getElementById('react')
);