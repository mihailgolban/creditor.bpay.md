const path = require("path");
const Dotenv = require('dotenv-webpack');
const MinifyPlugin = require("babel-minify-webpack-plugin");

module.exports = (env, argv) => {

    var mode = argv.mode;

    if (argv.mode === 'development') {
    }

    if (argv.mode === 'production') {
    }

    return {
        entry: "./public/scripts/react/index.js",
        output: {
            path: path.join(__dirname, "/public/scripts/react/dist"),
            filename: "bundle.js"
        },
        module: {
            rules: [{
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                },
            },
                {
                    test: /\.css$/,
                    use: ["style-loader", "css-loader"]
                }
            ]
        },
        plugins: [
            new MinifyPlugin({},{
                comments: false
            }),
            new Dotenv({
                path: `./.env.${mode}`, // load this now instead of the ones in '.env'
                safe: true, // load '.env.example' to verify the '.env' variables are all set. Can also be a string to a different file.
                systemvars: true, // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
                silent: true, // hide any errors
                defaults: false // load '.env.defaults' as the default values if empty.
            })
        ]

    };

};